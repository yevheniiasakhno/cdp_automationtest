﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test2.PageObject
{
    public class LoginPage
    {
        private IWebDriver _driver;

        public IWebElement Logotype => _driver.FindElement(By.XPath("//h3"));

        public IWebElement EmailInput => _driver.FindElement(By.CssSelector("input[name='email']"));

        public IWebElement PasswordInput => _driver.FindElement(By.CssSelector("input[name='password']"));

        public IWebElement SubmitButton => _driver.FindElement(By.CssSelector("#app > div > form > button > span"));

        public IWebElement ErrorLabel => _driver.FindElement(By.XPath("//p[@class='el-message__content']"));

        public LoginPage(IWebDriver driver)
        {
            _driver = driver;
        }

        public LoginPage EnterEmail(string email)
        {
            EmailInput.SendKeys(email);
            return this;
        }

        public LoginPage EnterPassword(string email)
        {
            PasswordInput.SendKeys(email);
            return this;
        }
        public string GetTitleText() => Logotype.Text;

        public string GetErrorLabelText() => ErrorLabel.Text;

        public DashboardPage Submit()
        {
            SubmitButton.Click();
            return new DashboardPage(_driver);
        }
    }
}
