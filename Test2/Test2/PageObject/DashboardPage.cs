﻿using OpenQA.Selenium;

namespace Test2.PageObject
{
    public class DashboardPage
    {
        private IWebDriver _driver;

        public IWebElement UserTitleLabel => _driver.FindElement(By.XPath("//div[@class='current-role']"));

        public DashboardPage(IWebDriver driver)
        {
            _driver = driver;
        }

        public string GetUserTitle() => UserTitleLabel.Text;
    }
}
