﻿using System;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using NUnit.Framework;
using System.Threading;
using Test2.PageObject;

namespace Test2
{
    class BackOfficeTests
    {
        IWebDriver driver;

        public LoginPage LoginPage { get; }

        public BackOfficeTests()
        {
            driver = new ChromeDriver();
            LoginPage = new LoginPage(driver);    
        }

        [SetUp]
        public void Open()
        {
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(15);
            driver.Navigate().GoToUrl("https://backoffice.staging.wya.jlpv.cloud/#/login");
        }

        [Test]
        public void LoginTest()
        {
            string userText = LoginPage
                .EnterEmail("yevheniia_sakhno@epam.com")
                .EnterPassword("TdDzCf1205*")
                .Submit()
                .GetUserTitle();

            Assert.That(userText, Is.EqualTo("SUPERUSER"));
        }

        [Test]
        public void FailedLoginTest()
        {
            LoginPage
                .EnterEmail("yevheniia_sakhno@epam.com")
                .EnterPassword("incorrect password")
                .Submit();

            Thread.Sleep(2000);

            string errorText = LoginPage.GetErrorLabelText();
            Assert.That(errorText, Is.EqualTo("Invalid credentials was provided"));
        }

        [Test]
        public void CheckVisibleTitle()
        {
            string titleText = LoginPage.GetTitleText();
            Assert.That(titleText, Is.EqualTo("WYA Admin Panel"));
        }

        [TearDown]
        public void CloseBrowser()
        {
            driver.Close();
        }
    }
}
